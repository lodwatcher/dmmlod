Installation Prerequisites:

- [Python 3.5+ (3.6+ preferred)](https://www.python.org/downloads/)
- [Git](https://git-scm.com/downloads)

Recommended:

- Python Virtual Environment
- FFmpeg
- [MKVMerge](https://mkvtoolnix.download/downloads.html)

## Installing and using dmmlod:

Download using git.

    git clone https://bitbucket.org/lodwatcher/dmmlod.git

(Optional) Create a [virtual environment](docs/virtualenv.md).

Install using pip.

    pip install -e dmmlod

Run setup. Answer [the questions](docs/headers.md).

    dmmlod setup

Download videos using save:

    dmmlod save <alias or playlist> [-n stage_name]

Merge:

    dmmlod merge path/to/saved/stage

Get help:

    dmmlod --help

[More options.](docs/options.md)

[Process merged videos](docs/processing.md).