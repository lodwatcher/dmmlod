# Extracting Headers and Cookies

# 3) Getting the right information

In order to download videos from DMM, you will need to use your browser's
developer tools to locate and copy several pieces of information:

- Your logged in browser's User-Agent header
- The playlist.m3u8 url for the video you wish to download
- The value of your current licenseUID cookie
- Optionally, the base64 encoded encryption key for the video in
question (you don't need it if you have the correct licenseUID)

In order to grab these, you need to open the developer window
(CTRL+SHIFT+I) as soon as the tab opens. Since that never happens,
you may need to refresh once. Then switch to the Network Tab.
(This works in Chrome and Firefox, I've no idea if it works in other browsers.)

***New versions of Chrome have a "feature" that hides headers on certain requests***.
You can disable it via chrome://flags/#site-isolation-trial-opt-out

## User-Agent
Check anything in the Network tab and look for the User-Agent header.
Copy that value and save it for when dmmlod setup asks later.

## LicenseUID
The easiest place to find the licenseUID is by looking through the
network tab for a request ending in "html5/". Check the cookies on that
request, and find the one named "licenseUID". Copy that value and store
it somewhere.

This value is NOT permanent, and WILL change, though when this happens
is not yet clear. Perhaps on every new login.

## Live stream playlists
Live streams for a given group come in three main qualities:
Standard Definition
High Definition
High Definition Fixed Camera (定點)

When you first open the streaming window for a live stream it will be
in standard quality. With the Network Tab open, find the button to
change quality, and change first to the highest displayed bitrate
without 定點. Find the newest "playlist.m3u8" in your network tab,
and copy that link. Save it somewhere. This is the High Definition Live playlist.

Now change the quality to the highest bitrate *with* 定點. Find the
latest playlist.m3u8, and save that link somewhere. This is the High
Definition Fixed Camera playlist.

Both of these playlists don't change often, if ever, so you can keep
reusing the same urls without checking the website, as long as they
continue working (if they stop working you should find the new ones).

When you run `dmmlod setup` later it will ask you for both of these
playlists, as well as your `licenseUID`. You can then start recording
them using short nicknames instead of pasting the whole long link.


## DMM and DMM HD recorded playlists
Every video has a different playlist. It's possible to obtain these
from the html5 endpoint, but I haven't implemented this in the script
yet so you'll have to do it by hand.

It's the same idea as the live stream playlists, except there's no
point keeping it once recording has finished. Just open your
Network Tab, switch to the highest bitrate, and copy the playlist.m3u8 url.

## Key
Immediately after fetching the first chunklist.m3u8, the player will
fetch the decryption key. The latest version of Chrome hides the
cookies etc. of this request, but they can be viewed in Firefox.
Either way what you actually need is the Preview (not the Response).
It should look something like `Gs345+kaA90aAAJKEe1R34==`
(24 characters, ending in two ==).

You only need this if you realise your licenseUID had expired *after*
you started recording a live stream. In such a case, if you can grab
the key at any time before the stream ends, you can still successfully
decrypt the video after it finishes downloading.

## Cookies, API, etc.
Future versions of the dmmlod script will attempt to make API calls to
obtain playlists, updated licenseUIDs, etc. It may be necessary to
copy other cookies or submitted information using the same methods.