# Virtual Environments

There are several methods available for creating a self-contained
virtual environment for python. The simplest is the built-in [venv](https://docs.python.org/3.6/library/venv.html#module-venv):

    python -m venv path/to/virtual/env

However activating an environment created with venv requires typing the
full path to the environment, which usually isn't desirable.

Two better alternatives are:

- [virtualenvwrapper](http://virtualenvwrapper.readthedocs.io/en/latest/install.html) (the author's preference)
- [pipenv](http://docs.python-guide.org/en/latest/dev/virtualenvs/)

If you do choose to use venv, I recommend first cloning dmmlod using git,
then inside the dmmlod directory run:

    python -m venv venv

This will create a new directory inside dmmlod called "venv", and you
just have to run:

    (bash) source venv/bin/activate
    (cmd.exe) venv/Scripts/activate.bat
    (PowerShell) venv/Scripts/Activate.ps1

from inside the dmmlod folder.

On the other had with virtualenvwrapper you can just run `workon dmmlod`
from anywhere, so...