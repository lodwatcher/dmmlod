# Processing

After merging, videos should be entirely playable, and if that's all
you want you can stop now. However, it's easier to make clips and seek
back and forth through an MP4, and live recordings have additional
"filler bytes" that can be removed to reduce the file size without
reducing quality.

## Live stream recordings
To remove filler bytes and remux to MP4:

### Linux:
    for f in *.ts; do
        mkvmerge --disable-track-statistics-tags "$f" -o "${f/ts/mkv}";
        ffmpeg -i "${f/ts/mkv}" -c copy -movflags +faststart "${f/ts/mp4}";
        rm "${f/ts/mkv}";
    done
Check the resulting mp4 for timing problems (e.g. audio out of sync with
video), but it should be fine.

### Windows
For a file named "akb48a17010101_livehd.ts" run the following in a terminal:

    mkvmerge --disable-track-statistics-tags "akb48a17010101_livehd.ts" -o "akb48a17010101_livehd.mkv"
    ffmpeg -i "akb48a17010101_livehd.mkv" -c copy -movflags +faststart "akb48a17010101_livehd.mp4"

## DMM HD recordings
DMM HD videos will misbehave if piped through mkvmerge first. However,
they contain no filler bytes, so it's fine to remux them to mp4 directly:

### Linux
    for f in *.ts; do
        ffmpeg -i "$f" -c copy -bsf:a aac_adtstoasc -movflags +faststart "${f/ts/mp4}";
    done
### Windows
For a file named "akb48a17010101_dmmhd.ts" run the following in a terminal:

    ffmpeg -i "akb48a17010101_dmmhd.ts" -c copy -bsf:a aac_adtstoasc -movflags +faststart "akb48a17010101_dmmhd.mp4"
