# All Commandline Options

## generic

These options apply to all commands.

    --license <license>, -L <license>

Overrides the stored licenseUID. Will also update config when used with `set`.

    --config-file <path/to/config>, -C <path/to/config>

Load settings from a different config file.

    --key <key>, -k <key>

Provide a base64 encoded decryption key. Most useful for `decrypt`

    --keyfile <path/to/keyfile>, -K <path/to/keyfile>

The path to a decoded decryption key file. Most useful for `decrypt`

    --savedir <path/to/save/directory>, -o <path/to/save/directory>

Overrides the output directory.


## dmmlod setup

Accepts any number of key=value pairs, which it will use to populate the
config file before asking questions. The value part can be properly
escaped JSON. For example:

    dmmlod setup save_directory=~/Downloads/DMM headers='{"User-Agent":"My Really Funny Browser/770.0"}'

If something goes wrong during setup, the existing configuration can be overriden using:

    --override

All questions will be re-asked, regardless of existing answers.

    --use-standard-config-path, -S

If the --config/-C generic option is used, read initial values from that config
file, but save the results to a standard location (varies depending on OS).

## dmmlod set

Accepts any number of key=value pairs, which it will use to update the config file.
The value part can be properly escaped JSON. Syntax is the same as `setup`

Will also write a new licenseUID given by the --license/-L generic option to file:

    dmmlod set -L "XXXXXXXXXXXXXXXXX=="


## dmmlod get

Displays current config fields.

    dmmlod get <field_name> [<field_name2> [...]]

Use "all" to show all fields:

    dmmlod get all

To avoid excessive clutter, `get` by default only displays the top value,
not the contents of that value. If you wish to display e.g. playlist urls
and full header contents, use:

    --show-full, -F

Thus, this:

    dmmlod get -F all

Will show every value of every field down to the lowest level.

## dmmlod save

save takes one positional argument: the playlist or playlist alias to download.
That is, either the URL of the playlist.m3u8, or a playlist
name to load from config, like "livehd". Automatically
downloads the best available stream. Note that fixed
camera and hd use different playlist urls from the
default sd stream.

    --name <name>, -n <name>

What to call the folder that downloaded segments are saved in. Will be generated
automatically if not provided (but you should provide it, the auto names are garbage.)

    --skip-exists, -x

Skip existing segments. Use to fix errors that happen while downloading
VODs (e.g. DMM HD). Won't work properly (or at all) if you don't provide a name.

    --merge, -M

Automatically merge the finished file. Be careful of using this, as it
will still try to merge even if there are download errors.


## dmmlod decrypt

If decryption fails during `save`, but you manage to extract the key
from the website before the stream ends, the downloaded files can
still be decrypted using this command.

Takes two positional arguments:

- **source-directory**: the directory containing the downloaded, encrypted segments
- **dest-directory**: the directory to save decrypted segments to

Provide the key using the --key/-k or --keyfile/-K generic options.

## dmmlod merge

Merge downloaded + decrypted segments into a single ts file.

Takes two positional arguments:

- **source-directory**: the directory containing the downloaded, decrypted segments
- **dest-file** (optional): File name for the merged TS file.
If not provided, will use the name of the source directory
