import datetime
import re
import glob
from .constants import TOKYO_TZ

video_chunk_re = re.compile(r'media_b\d+_(\d+).ts')
SKIP_KEYS = (
    'AMP_TOKEN', 'BASKET_Cookie', 
    '__utma', '__utmb', '__utmc', '__utmt', 
    '_dc_gtm_UA-48257133-1', '_dga', '_dga_gid', '_gat_mutual', '_gcl_au', '_gid', 
    '_td', 'cX_G', 'cX_P', 'cX_S', 'cdp_id', 'cklg', 
    'd_mylibrary', 'digital[play_muted]', 'digital[play_volume]', 
    'dtmd', 'dtmh', 'dtms', 'dtmu', 
    'has_althash', 'i3_ab', 'i3_opnd', 'i3_recommend_ab', 
    'mbox', 's_pers', 's_sess', 'search_words',
    'check_item_history_ver3', 'digi_his', '_ref1552810400', '_clicks',
)
KEYS_NEEDED = (
    'INT_SESID', 'app_uid', 'check_done_login', 'ckcy', 'dmm_service', 'licenseUID', 'login_session_id', 'd_certify', 
)


def fromisoformat(date_str):
    """
    currently only supports YYYY-MM-DD"""
    year, month, day = date_str.split('-')
    return datetime.datetime(year=int(year), month=int(month), day=int(day), tzinfo=TOKYO_TZ)


def format_date(dt=None, long_date=False):
    if not dt:
        dt = datetime.datetime.now(tz=TOKYO_TZ)

    if long_date:
        return '{:04d}-{:02d}-{:02d}_{:02d}:{:02d}'.format(dt.year, dt.month, dt.day, dt.hour, round(dt.minute/30)*30)
    else:
        return '{:02d}{:02d}{:02d}{:02d}{:02d}'.format(dt.year%100, dt.month, dt.day, dt.hour, round(dt.minute/30)*30)


def get_media_sequence(file):
    return int(video_chunk_re.search(file).group(1))


def extract_filename(url):
    return video_chunk_re.search(url).group(0)


def sort_segments(segment_list):
    segment_list.sort(key=lambda x: get_media_sequence(x))


def split_cookie_string(cookie_string):
    if cookie_string.startswith('Cookie: '):
        cookie_string = cookie_string[len('Cookie: '):]
    return dict(e.split('=', 1) for e in cookie_string.split('; '))


def prune_cookies(cookies):
    for bad_key in SKIP_KEYS:
        try:
            cookies.pop(bad_key)
        except KeyError:
            pass


def check_segments(path):
    def sort_key(filename):
        return int(video_chunk_re.search(filename).group(1))

    files = glob.glob(f'{path}/*.ts')
    files.sort(key=sort_key)

    start_segment_index = sort_key(files[0])
    final_segment_index = sort_key(files[-1])
    expected_segments = set(range(start_segment_index, final_segment_index))
    found_segments = set(sort_key(file) for file in files)
    missing = expected_segments - found_segments

    return sorted(missing)
