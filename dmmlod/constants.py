from io import DEFAULT_BUFFER_SIZE as _DEFAULT_BUFFER_SIZE
import os
import datetime

# These are used by the client to fill in missing config values
DEFAULT_SAVE_DIRECTORY = os.path.expanduser('~/Downloads/DMM')
DEFAULT_HEADERS = {
    "User-Agent":
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) "
        "AppleWebKit/537.36 (KHTML, like Gecko) "
        "Chrome/58.0.3029.110 Safari/537.36"
}

# while it's possible to set these in a config file, doing so is undocumented
DEFAULT_WORKER_COUNT = 6
DEFAULT_CHUNKSIZE=_DEFAULT_BUFFER_SIZE

MAX_404_ERRORS = 5
MAX_ATTEMPTS = 5
TIMEOUT = 10

TOKYO_TZ = datetime.timezone(datetime.timedelta(hours=9), name='Asia/Tokyo')
