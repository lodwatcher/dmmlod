"""
Functions to find, load, and save config files.

The config file is searched for as follows:
1) path parameter
2) current directory
3) user home folder
(windows) %USER%\lod.conf
(linux) $HOME/lod.conf

4) dmmlod data folder, i.e.
(windows) %APPDATA%\DMMLOD\lod.conf
(linux) ~/.config/dmmlod/lod.conf

When saving the current configuration, always chooses #4 unless given an
explicit path. This might result in oddities, and future versions may
decide to mimic load's order instead, overwriting the first file found.

Another option would be for load to go in reverse order and update the config
with data from each file found, overriding the existing data. But this would require
deep update, and I don't feel like messing with that nonsense right now.
"""
import yaml
import os
import appdirs


def load(path=None, ignore_errors=False):
    """

    :param path:
    :return:
    """
    try:
        configfile = _find_config(path)
    except FileNotFoundError:
        if ignore_errors:
            return {}
        else:
            raise

    with open(configfile, encoding='utf8') as infp:
        # TODO: does this require any further processing?
        data = yaml.safe_load(infp)

    return data


def save(data, path=None):
    path = _find_config(path, use_last=True)

    with open(path, 'w', encoding='utf8') as outfp:
        yaml.dump(data, outfp, indent=4, width=79)


def _find_config(path=None, use_last=False):
    if path:
        path = os.path.expanduser(path)
        if os.path.isfile(path):
            return path
        elif os.path.isdir(path):
            return os.path.join(path, 'dmmlod.conf')
    else:
        for path in (
                'dmmlod.conf',
                os.path.expanduser('~/dmmlod.conf'),
                os.path.join(appdirs.user_config_dir('dmmlod'), 'dmmlod.conf')
        ):
            if os.path.isfile(path):
                return path

    if use_last:
        return path
    else:
        raise FileNotFoundError('Could not locate config file at {}'.format(path))


def find_config():
    """
    Locate the current config file.

    This is intended for user reference, load uses another function, though the
    result will be the same.

    :return: path to config file
    """
    return _find_config(use_last=True)