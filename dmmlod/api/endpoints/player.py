import re
import json
import time

__all__ = ["PlayerMixin"]

_player_args_re = re.compile(r'args = ({.+?});', flags=re.DOTALL)


def extract_args(text):
    for old, new in (('\\u005f', '_'), ('\\u002f', '/'), ('\\u003d', '=')):
        text = text.replace(old, new)

    m = _player_args_re.search(text)
    if not m:
        raise ValueError('Couldn\'t find args in text')

    args = m.group(1)
    for var, val in (
            # TODO: actually find these values in the page
            ('API_FORMAT', '"json"'),
            ('API_MAIL_FLAG', '"yes"')):
        args = args.replace(var, val)

    # is there a better way to do this?
    args = re.sub(r'\s+', ' ', args)
    args = re.sub(r'{ ', '{"', args)
    args = re.sub(r', }', r'}', args)
    args = re.sub(' :', '":', args)
    args = re.sub(', ', ', "', args)

    try:
        return json.loads(args)
    except json.decoder.JSONDecodeError:
        print(args)


class PlayerMixin:
    def get_playlist_urls(self, cid, channel=None, exploit_id=None):
        if not channel:
            channel = cid[:5]
        # the final character might be different for some edge cases
        url = 'https://www.dmm.com/monthly/-/player/=/player=html5/act=playlist/channel={}/pid={}m'.format(channel, cid)
        # again this might need to be adjusted for rod
        referer = 'https://www.dmm.com/lod/{}/-/detail/=/cid={}/'.format(channel, cid)

        # this can throw a ConnectionError
        r = self.session.get(url, headers={"Referer": "https://www.dmm.com/lod/akb48/-/detail/=/cid=akb48c18101601/"})

        if not r.ok:
            # handle error conditions
            r.raise_for_status()

        try:
            args = extract_args(r.text)
        except ValueError as e:
            with open('test_read.html', 'wb') as outfp:
                outfp.write(r.content)
            print(url)
            import os
            print(os.getcwd(), 'test_read.html', sep='/')
            raise e

        referer = url
        api_url = 'https://www.dmm.com/service/digitalapi/-/html5/'

        api_headers = {"Referer": referer, "X-Requested-With": "XMLHttpRequest"}
        # first post request, using the args from the player page
        r = self.session.post(
            api_url, 
            json=args,
            headers=api_headers
        )


        info = r.json()
        # what happens if there's more than one item in the playlist?

        results = []
        for item in info['list']['item']:
            time.sleep(0.1)
            payload = {}
            for key in ("action", "service"):
                payload[key] = info[key]

            payload['format'] = args['format']
            payload['mail'] = args['mail']

            for key in ('index', 'shop_name', 'product_id', 'part', 'media', 'category', 'parent_product_id'):
                payload[key] = item[key]

            # print('Fetching playinfo with', json.dumps(payload, indent=2))
            r = self.session.post(api_url, json=payload, headers=api_headers)
            if not r.ok:
                # handle error conditions
                r.raise_for_status()

            data = r.json()
            best = sorted(data['list']['item'], key=lambda x: int(x['bitrate']))[-1]
            results.append({
                "cid": cid,
                "title": item["name"],
                "index": item['index'],
                "bitrate": best['bitrate'],
                "playlist": best['url']
            })

        
        return results




