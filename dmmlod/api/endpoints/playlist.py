import os
import time
import binascii
import itertools
import datetime
from multiprocessing import Pool as ProcessPool, Process, JoinableQueue as ProcessQueue

import requests
import m3u8
from Crypto.Cipher import AES

from dmmlod.utils import format_date, extract_filename, get_media_sequence, check_segments, fromisoformat
from dmmlod.constants import (
    TIMEOUT, TOKYO_TZ,
    DEFAULT_SAVE_DIRECTORY, DEFAULT_HEADERS, DEFAULT_WORKER_COUNT, DEFAULT_CHUNKSIZE,
    MAX_ATTEMPTS, MAX_404_ERRORS,
)


def _worker(inq, outq, func):
    while True:
        item = inq.get()
        if item is None:
            break
        try:
            func(*item)
        except Exception as e:
            print(e, *item, sep='\n')
            outq.put(item)
        inq.task_done()


class PlaylistMixin:
    def save_playlist(self, playlist, savedir=None, name=None, skip_exists=False, key=None):
        """
        Download and if possible decrypt video segments from a playlist

        TODO:
        - allow selecting a specific bitrate instead of just grabbing the best
        - don't do more than the minimal processing necessary before handing off to one of the secondary save methods
          e.g. figure out if it's live or not before loading the chunklist, if that's possible
        - allow starting from a chunklist url instead of a playlist one

        :param playlist:
        :param savedir:
        :param name:
        :param skip_exists:
        :param key:
        :return:
        """
        if playlist in self.config.get('playlists', {}):
            playlist_url = self.config['playlists'][playlist]
        else:
            # TODO: check if valid url
            playlist_url = playlist

        if not name:
            group = self.config.get('group')
            label = playlist if playlist != playlist_url else 'lod'
            date = format_date()
            name = '_'.join((e for e in (group, date, label) if e))

        savedir = os.path.join((savedir or self.config.get('save_directory', DEFAULT_SAVE_DIRECTORY)), name)
        os.makedirs(savedir, exist_ok=True)

        # base_uri = playlist_url.rsplit('/', 1)[0] + '/'

        errors = 0
        while errors < self.config.get('max_attempts', MAX_ATTEMPTS):
            try:
                r = requests.get(playlist_url, headers=self.config.get('headers', DEFAULT_HEADERS))
                r.raise_for_status()
            except requests.exceptions.HTTPError as e:
                if e.response.status_code == 404:
                    print('Caught 404 while fetching initial playlist')
                    errors += 1
                    time.sleep(errors)
                    continue
                else:
                    raise
            except requests.exceptions.ConnectionError:
                errors += 1
                time.sleep(max(3, errors+1))
                continue
            else:
                break
        else:
            print('Unable to fetch initial playlist')
            return None

        # update base uri based on redirect
        base_uri = r.url.rsplit('/', 1)[0] + '/'

        playlist_obj = m3u8.loads(r.text)
        playlist_obj.base_uri = base_uri
        # get best chunklist
        best_variant = sorted(playlist_obj.playlists, key=lambda x: x.stream_info.bandwidth)[-1]

        errors = 0
        chunklist_url = best_variant.absolute_uri
        while errors < self.config.get('max_attempts', MAX_ATTEMPTS):
            try:
                r = requests.get(chunklist_url, headers=self.config.get('headers', DEFAULT_HEADERS))
                r.raise_for_status()
            except requests.exceptions.HTTPError as e:
                if e.response.status_code == 404:
                    print('Caught 404 while fetching first chunklist')
                    errors += 1
                    time.sleep(errors)
                    continue
                else:
                    raise
            except requests.exceptions.ConnectionError:
                errors += 1
                time.sleep(max(3, errors+1))
                continue
            else:
                break
        else:
            print('Unable to fetch first chunklist')
            return None

        chunklist_obj = m3u8.loads(r.text)
        # TODO: check response headers for cookies etc.

        chunklist_obj.base_uri = base_uri

        keyfile = '{}.key'.format(name)
        if not key:
            keyuri = chunklist_obj.keys[0].uri
            decryption_key = self.save_decryption_key(keyuri, savedir, keyfile)
        else:
            decryption_key = key

        if chunklist_obj.is_endlist:
             final_obj = self.save_seekable_playlist(chunklist_obj,
                                                     savedir,
                                                     key=decryption_key,
                                                     skip_exists=skip_exists)
        else:
            # TODO: catch errors
            final_obj = self.save_live_playlist(chunklist_url, chunklist_obj, savedir, key=decryption_key)
            final_obj.is_endlist = True

        # have to deal with the key and merging manually
        playlist_file_path = '{}/{}.m3u8'.format(savedir, name)

        # if not decryption_key:
        #     final_obj.keys[0].uri = keyfile

        text = final_obj.dumps()
        self.save_m3u8(text, playlist_file_path, strip_keys=bool(decryption_key))

        return name

    @staticmethod
    def save_m3u8(body, dest, strip_keys=False):
        with open(dest, 'w', encoding='utf8') as outfp:
            if strip_keys:
                for line in body.split('\n'):
                    if 'EXT-X-KEY' in line:
                        continue
                    outfp.write(line)
                    outfp.write('\n')
            else:
                outfp.write(body)

    def save_decryption_key(self, url, savedir, keyfile):
        fullpath = os.path.join(savedir, keyfile)
        licenseUID = self.config.get('licenseUID')
        if not licenseUID:
            return None

        try:
            r = requests.get(url,
                             headers=self.config.get('headers', DEFAULT_HEADERS),
                             cookies={"licenseUID": licenseUID})
        except requests.exceptions.ConnectionError:
            pass
        else:
            if r.status_code == 200:
                key = r.content
                if len(key) > 32:
                    pass
                else:
                    with open(fullpath, 'wb') as outfp:
                        outfp.write(key)
                    return key

        print('Failed to retrieve key from {}'.format(url))

    def save_seekable_playlist(self, chunklist_obj, savedir, key, skip_exists):
        def get_output_path(url):
            return os.path.join(savedir, extract_filename(url))

        segment_urls = [e.absolute_uri for e in chunklist_obj.segments]
        segment_paths = [os.path.join(savedir, extract_filename(e)) for e in segment_urls]

        if skip_exists:
            segment_urls = [e for e in segment_urls if not os.path.exists(get_output_path(e))]

        print(savedir)
        args = zip(segment_urls, (get_output_path(e) for e in segment_urls), itertools.repeat(key))
        pool = ProcessPool(self.config.get('worker_count', DEFAULT_WORKER_COUNT))

        pool.starmap(self.save_segment, args, chunksize=1)
        pool.close()

        for segment in chunklist_obj.segments:
            segment.uri = extract_filename(segment.uri)

        return chunklist_obj

    def save_live_playlist(self, chunklist_url, chunklist_obj, savedir, key=None):
        errors = 0
        ts_queue = ProcessQueue()
        error_queue = ProcessQueue()
        known_segments = set()
        base_uri = chunklist_url.rsplit('/', 1)[0] + '/'
        print(savedir)
        target_duration = int(chunklist_obj.target_duration)

        workers = []
        for i in range(self.config.get('worker_count', DEFAULT_WORKER_COUNT)):
            p = Process(target=_worker, args=(ts_queue, error_queue, self.save_segment))
            p.start()
            workers.append(p)

        def sleep(sleepdt):
            time.sleep(max((sleepdt - datetime.datetime.now()).total_seconds(), 0))

        sleepdt = datetime.datetime.now() + datetime.timedelta(seconds=target_duration)
        for segment in chunklist_obj.segments:
            url = segment.absolute_uri
            filename = extract_filename(url)
            dest = os.path.join(savedir, filename)
            print('Saving {}'.format(filename))
            ts_queue.put((url, dest, key))
            segment.uri = filename
            known_segments.add(filename)
        sleep(sleepdt)

        while errors < self.config.get('max_404_errors', MAX_404_ERRORS):
            sleepdt = datetime.datetime.now() + datetime.timedelta(seconds=target_duration)
            try:
                r = requests.get(chunklist_url, headers=self.config.get('headers', DEFAULT_HEADERS))
            except (requests.exceptions.ConnectionError, requests.exceptions.ChunkedEncodingError):
                time.sleep(min(errors, target_duration)+1)
                continue
            try:
                r.raise_for_status()
            except requests.exceptions.HTTPError:
                if r.status_code == 404:
                    print('Caught 404 while fetching chunklist')
                    errors += 1
                    time.sleep(min(errors, target_duration))
                    continue
                elif r.status_code >= 500:
                    errors += 1
                    time.sleep(min(errors, target_duration))
                    continue
                raise

            errors = 0
            partlist = m3u8.loads(r.text)
            partlist.base_uri = base_uri
            for segment in partlist.segments:
                filename = extract_filename(segment.uri)
                if filename in known_segments:
                    continue
                url = segment.absolute_uri
                segment.key = None  # clear keys on new segments
                dest = os.path.join(savedir, filename)
                print('Saving {}'.format(filename))
                ts_queue.put((url, dest, key))
                segment.uri = filename
                chunklist_obj.add_segment(segment)
                known_segments.add(filename)
            sleep(sleepdt)  # the website refetches the chunklist every 15 seconds

        ts_queue.join()

        # TODO: process the error queue
        lost_files = []
        while not error_queue.empty():
            item = error_queue.get()
            filename = extract_filename(item[0])
            lost_files.append(filename)
            error_queue.task_done()

        if lost_files:
            print('Errors:', *lost_files, sep='\n')

        missing = check_segments(savedir)
        if missing:
            print('Missing segments numbered:', *missing)

        for i in range(DEFAULT_WORKER_COUNT):
            ts_queue.put(None)
        for worker in workers:
            worker.join()

        return chunklist_obj

    def save_segment(self, url, destfile, key=None, iv=None):
        chunksize = self.config.get('chunksize', DEFAULT_CHUNKSIZE)
        exc = None

        for attempt in range(self.config.get('max_attempts', MAX_ATTEMPTS)):
            time.sleep(attempt)
            try:
                r = requests.get(url, headers=self.config.get('headers', DEFAULT_HEADERS),
                                 timeout=self.config.get('timeout', TIMEOUT), stream=True)
                r.raise_for_status()
            except (requests.exceptions.HTTPError, requests.exceptions.ConnectionError) as e:
                # assume 404
                exc = e
                status = e.response.status_code
                continue

            if r.status_code != 200:
                if r.status_code == 404:
                    print('Got 404 when fetching {}'.format(url))
                    print(r.headers, r.url)
                    r.raise_for_status()
                    # status = r.status_code
                    # exc = None
                continue

            if key:
                if not iv:
                    iv = hex(get_media_sequence(url))[2:]
                    iv = "0" * (32 - len(iv)) + iv
                    iv = binascii.unhexlify(iv)
                crypt_mode = AES.MODE_CBC
                decryptor = AES.new(key, crypt_mode, IV=iv)
                with open(destfile, 'wb') as outfp:
                    try:
                        cryptchunk = b''
                        for chunk in r.iter_content(chunk_size=chunksize):
                            # TODO: catch decryption errors
                            cryptchunk += chunk
                            if len(cryptchunk) % 16 == 0:
                                plainchunk = decryptor.decrypt(cryptchunk)
                                outfp.write(plainchunk)
                                cryptchunk = b''
                        if len(cryptchunk):
                            raise ValueError('{} bytes left in encrypted stream: {}'.format(len(cryptchunk),
                                                                                            os.path.basename(destfile)))
                    except (requests.exceptions.Timeout,
                            requests.exceptions.ConnectionError,
                            requests.exceptions.ChunkedEncodingError) as e:
                        # TODO: handle errors
                        # print(destfile, e)
                        exc = e
                        continue
                    # "quick" fix for decryption errors
                    except ValueError as e:
                        # print(destfile, e)
                        exc = e
                        continue
                    else:
                        break
                    finally:
                        r.close()
            else:
                with open(destfile, 'wb') as outfp:
                    try:
                        for chunk in r.iter_content(chunk_size=chunksize):
                            if chunk:
                                outfp.write(chunk)
                    except (requests.exceptions.Timeout,
                            requests.exceptions.ConnectionError,
                            requests.exceptions.ChunkedEncodingError) as e:
                        print(destfile, e)
                        exc = e
                        continue
                    else:
                        break
                    finally:
                        r.close()
        else:
            try:
                os.remove(destfile)
            except FileNotFoundError:
                pass
            print('Download of', os.path.basename(destfile), 'failed with error:', exc)
            if exc:
                raise exc

    def schedule(self, playlist, datestr=None, timestr=None, name=None, savedir=None):
        if not datestr:
            dt = datetime.datetime.now(tz=TOKYO_TZ)
        else:
            dt = fromisoformat(datestr)
        if not timestr:
            tt = datetime.time(hour=18, minute=30)
        else:
            hour, minute = timestr.split(':')
            tt = datetime.time(hour=int(hour), minute=int(minute))

        startdt = datetime.datetime.combine(dt, tt, tzinfo=TOKYO_TZ)

        targetdt = startdt - datetime.timedelta(minutes=4)
        print('Starting recording at', targetdt.astimezone().isoformat(sep=' ', timespec='minutes'))

        while True:
            currdt = datetime.datetime.now(tz=TOKYO_TZ)
            if currdt > targetdt:
                break
            else:
                time.sleep(15)

        return self.save_playlist(playlist=playlist, name=name, savedir=savedir)
