"""
http://www.dmm.com/lod/akb48/-/list/
http://www.dmm.com/lod/akb48/-/list/=/grade=hd/

http://www.dmm.com/lod/rod/-/list/=/group=akb48/
http://www.dmm.com/lod/rod/-/list/=/group=ske48/
http://www.dmm.com/lod/rod/-/list/=/group=nmb48/
http://www.dmm.com/lod/rod/-/list/=/group=hkt48/
http://www.dmm.com/lod/rod/-/list/=/group=ngt48/
http://www.dmm.com/lod/rod/-/list/=/group=ngt48/limit=30/
http://www.dmm.com/lod/rod/-/list/=/group=ngt48/limit=60/
http://www.dmm.com/lod/rod/-/list/=/group=ngt48/limit=120/ (default)
http://www.dmm.com/lod/rod/-/list/=/group=ngt48/view=text/
http://www.dmm.com/lod/rod/-/list/=/group=ngt48/limit=60/view=text/
# all groups
http://www.dmm.com/lod/rod/-/list/=/sort=date/
http://www.dmm.com/lod/rod/-/list/=/sort=performance_date/page=2/
http://www.dmm.com/lod/rod/-/list/=/sort=performance_date/
http://www.dmm.com/lod/rod/-/list/=/group=akb48/limit=60/sort=performance_date/view=text/
http://www.dmm.com/lod/rod/-/list/=/group=akb48/sort=performance_date/team=a/view=text/
akb48: a, k, b, f, ei, r
ske48: s, k2, e, r2
nmb48: n, m, b2, r4, ca (cataleya-gumi)
hkt48: h, k4, t2, r3, sf (himawari-gumi)
ngt48: n3, r5

http://www.dmm.com/lod/rod/-/list/=/group=ngt48/performance_year=2009/sort=date/view=text/
http://www.dmm.com/lod/rod/-/list/=/group=ngt48/performance_month=1/performance_year=2009/sort=date/view=text/
http://www.dmm.com/lod/rod/-/list/=/grade=hd/group=ngt48/performance_month=1/performance_year=2009/sort=date/team=n3/view=text/
http://www.dmm.com/lod/rod/-/list/=/group=akb48/performance_year=2016/sort=date/view=text/page=2/

http://www.dmm.com/lod/akb48/-/list/

http://www.dmm.com/lod/rod/-/list/=/group=akb48/performance_year=2016/sort=date/

http://www.dmm.com/lod/akb48/-/list/=/article=akb48member/id=655/sort=date/
name, english spelling, agency, nickname, birthdate, birthplace, link to official profile
list of current stages the member is appearing in

http://www.dmm.com/lod/akb48/-/list/=/article=team/team=a/
http://www.dmm.com/lod/akb48/-/list/=/article=team/team=k/
http://www.dmm.com/lod/akb48/-/list/=/article=team/team=b/
http://www.dmm.com/lod/akb48/-/list/=/article=team/team=f/
http://www.dmm.com/lod/akb48/-/list/=/article=team/team=ei/
http://www.dmm.com/lod/akb48/-/list/=/article=team/team=r/

http://www.dmm.com/lod/akb48/-/akb48member/
http://www.dmm.com/lod/ske48/-/ske48member/
http://www.dmm.com/lod/nmb48/-/nmb48member/
http://www.dmm.com/lod/hkt48/-/hkt48member/
http://www.dmm.com/lod/ngt48/-/ngt48member/



Ended performances:
http://www.dmm.com/lod/akb48/-/end-list/
This is the jackpot!!!
There are even still stage comments available!!!!
And for those stages that have comments, member lists!!!!
It's only available for AKB though...

"""

import re
import requests
from bs4 import BeautifulSoup


_groups_to_teams = {
    "akb48": ('a', 'b', 'k', 'f', 'ei', 'r', 'c', 'd'),
    "ske48": ('s', 'k2', 'e', 'r2'),
    "nmb48": ('n', 'm', 'b2', 'r4', 'ca'),
    "hkt48": ('h', 'k4', 't2', 'r3', 'sf'),
    "ngt48": ('n3', 'g', 'r5'),
    "stu48": (),
}
_cid_re = re.compile(r'/cid=([\w\d]+)/')
_date_re = re.compile(r'(\d{4})年(\d+)月(\d+)日')
_pagenav_re = re.compile(r'/page=(\d+)/')

# TODO: move all this to a utils file?
# or merge this with the search module?
def _get_page_count(soup):
    pagenav = soup.select_one('div.list-boxpagenation').select('li')
    # TODO: make this more intelligent, e.g. check for li.terminal
    # if no li.terminal, take the second to last link

    # case 1: one page -- no page links
    if len(pagenav) == 1:
        return 1
    # case 2: two pages -- next button is the last page
    elif len(pagenav) == 3:
        return 2
    # case 3: three pages -- next button isn't the last page
    elif len(pagenav) == 4:
        return 3
    # case 4: 4 or more pages -- next and last buttons
    else:
        m = _pagenav_re.search(pagenav[-1].a['href'])
        if not m:
            print('Couldn\'t find page count:', pagenav[-1].a['href'])
            raise ValueError
        return int(m.group(1))


def _parse_performance_entry(item):
    url = item.a['href']
    m = _cid_re.search(url)
    if not m:
        raise ValueError('Couldn\'t locate cid in {}'.format(url))
    cid = m.group(1)
    title = item.img['alt']

    if 'HD' in item.text:
        hd_available = True
    else:
        hd_available = False

    group = cid[:5]
    # rod entries also have a group icon that could be useful

    return dict(
        date=_parse_date(title),
        cid=cid,
        url=url,
        title=title,
        group=group,
        hd_available=hd_available,
        image_url=item.img['src'],
    )


def _parse_date(title):
    m = _date_re.search(title)
    if not m:
        print('Failed to find date in title:', title)
        return ""
    return "{:04d}-{:02d}-{:02d}".format(*(int(e) for e in m.groups()))


class PerformanceListMixin:
    def get_group_performance_list(self, group, hd_only=False):
        """
        Fetch a list of stages available on a group's LOD page

        :param group: The group to fetch, one of AKB48, SKE48, NMB48, HKT48, NGT48 (case insensitive)
        :param hd_only: whether to only return performances available in HD

        :return: yields dictionaries containing the following fields:
            cid             content id
            url             url to the stage detail page
            title           date, weekday, title, and event
            hd_available    whether hd (1080p for regular, 720p for ROD) is available
            image_url       cover image for the stage
        """
        # it is possible to get performances by team:
        # http://www.dmm.com/lod/akb48/-/list/=/article=team/team=a/
        group = group.lower()

        if group not in _groups_to_teams:
            # TODO: raise error on invalid group
            return None

        if hd_only:
            url = 'https://www.dmm.com/lod/{}/-/list/=/grade=hd/'.format(group)
        else:
            url = 'https://www.dmm.com/lod/{}/-/list/'.format(group)

        r = self.session.get(url)

        if not r.ok:
            # TODO: handle status errors
            # also handle errors during the get (connection etc.)
            return None

        # TODO: use social48.markup instead? that way lxml vs. html.parser etc.
        # can be determined in one location instead of hardcoded here
        soup = BeautifulSoup(r.text, 'lxml')

        for item in soup.select('dl.prod'):
            yield _parse_performance_entry(item)

        # check for multiple pages
        pagination = soup.select_one('div.paginationControl')
        pages = pagination.select('a')[:-1]  # remove the next page link
        for page in pages:
            url = page['href']
            r = self.session.get(url)

            if not r.ok:
                # TODO: handle status errors
                # also handle errors during the get (connection etc.)
                return None

            # TODO: use social48.markup instead? that way lxml vs. html.parser etc.
            # can be determined in one location instead of hardcoded here
            soup = BeautifulSoup(r.text, 'lxml')

            for item in soup.select('dl.prod'):
                yield _parse_performance_entry(item)


    def get_rod_performance_list(self,
                                 group=None, team=None,
                                 year=None, month=None,
                                 hd_only=False, sort_by_upload_date=False,
                                 ):
        """
        Fetch a list of stages available from REVIVAL!! ON DEMAND

        :param group:
        :param team:
            akb48: a, k, b, f, ei, r
            ske48: s, k2, e, r2
            nmb48: n, m, b2, r4, ca (cataleya-gumi)
            hkt48: h, k4, t2, r3, sf (himawari-gumi)
            ngt48: n3, r5

            "r" can be used as an alias for all research student stages

            group is required when team is provided

        :param year:                    4-digit year
        :param month:                   1- or 2- digit month
        :param hd_only:                 only return stages available in HD
        :param sort_by_upload_date:     whether to sort by upload date instead of performance date

        :return: yields dictionaries containing the following fields:
            cid             content id
            url             url to the stage detail page
            title           date, weekday, title, and event
            hd_available    whether hd (1080p for regular, 720p for ROD) is available
            image_url       cover image for the stage
        """
        url = 'https://www.dmm.com/lod/rod/-/list/'
        args = []

        if hd_only:
            args.append('grade=hd/')

        args.append('limit=120/')
        if group:
            group = group.lower()
            if group not in _groups_to_teams:
                raise ValueError('Unknown group: {}'.format(group.upper()))
            args.append('group={}/'.format(group))
        if month:
            if not int(month) in range(1,13):
                raise ValueError('Unknown month: {}'.format(month))
            args.append('performance_month={}/'.format(month))
        if year:
            # TODO: get current year from datetime or something
            if not int(year) in range(2009, 2020):
                raise ValueError('Unknown year: {}'.format(year))
            args.append('performance_year={}/'.format(year))
        if sort_by_upload_date:
            # technically this is the default, but it seems to get added to the url more often than not
            args.append('sort=date/')
        else:
            args.append('sort=performance_date/')
        if team:
            if not group:
                # TODO: automatically determine group?
                raise ValueError('You must include a group to limit by team.')
            team = team.lower()
            # TODO: handle converting e.g. KIV to k4, etc.
            if team not in _groups_to_teams.get(group, ()):
                if team == 'r':
                    team = next(t for t in _groups_to_teams.get(group, ()) if t.startswith('r'))
                else:
                    raise ValueError('Team {} not found for group {}'.format(team.upper(), group.upper()))
            args.append('team={}/'.format(team))

        if args:
            url = ''.join((url, '=/', *args))

        print(url)
        r = self.session.get(url)
        if not r.ok:
            # TODO: handle status errors
            r.raise_for_status()

        soup = BeautifulSoup(r.text, 'lxml')
        page_count = _get_page_count(soup)
        # read additional pages if any
        # this is more complicated.
        # anything over 3 pages long with have a last button as well as a next button
        # and some of the pages will be elided (...)
        # the number of visible pages seems to vary
        # but it seems I do need to actually calculate how many pages are available
        print(page_count)
        # pages = (*(p.a['href'] for p in pages[1:-1]),)
        for i in range(page_count):
            if i:
                page_url = '{}/page={}/'.format(url, i+1)
                r = self.session.get(page_url)
                if not r.ok:
                    print(page_url)
                    r.raise_for_status()
                    # TODO: handle status errors
                soup = BeautifulSoup(r.text, 'lxml')

            for item in soup.select('ul#list > li'):  # ('.tmb'):
                yield _parse_performance_entry(item)

    def get_performance_detail(self, cid=None, url=None, rod=False):
        """
        Retrieves and parses the detail page for a performance, given either
        a direct url, or cid + group (and optionally rod).

        :param cid:   performance content id
        :param group: group
        :param url:   direct url (no other information needed and will be ignored)
        :param rod:   whether the stage is on ROD rather than a group page
        :return:
        """
        if cid:
            """
            https://www.dmm.com/lod/rod/-/detail/=/cid=akb48b16091601/
            https://www.dmm.com/lod/akb48/-/detail/=/cid=akb48r18073001/
            """
            if rod:
                url = 'https://www.dmm.com/lod/rod/-/detail/=/cid={}'.format(cid)
            else:
                group = cid[:5]
                url = 'https://www.dmm.com/lod/{}/-/detail/=/cid={}/'.format(group, cid)
        elif not url:
            raise ValueError('Either cid or url is required.')

        r = self.session.get(url)
        if not r.ok:
            # TODO: handle errors
            r.raise_from_status()

        # TODO: finish this method
