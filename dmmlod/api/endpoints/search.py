"""
http://www.dmm.com/search/=/searchstr=武藤十夢/
This is limited to just ROD performances by Tomu, but how? what do these three keys mean?
http://www.dmm.com/search/=/searchstr=武藤十夢/n1=FgRCTw9VBA4GCV5c/n2=Aw1fVhQKX15XAW5KXFI_/
http://www.dmm.com/search/=/searchstr=村山彩希/n1=FgRCTw9VBA4GCV5c/n2=Aw1fVhQKX15XAW5KXFI_/
http://www.dmm.com/search/=/searchstr=谷口めぐ/n1=FgRCTw9VBA4GCV5c/n2=Aw1fVhQKX15XAW5KXFI_/
http://www.dmm.com/search/=/searchstr=武藤十夢/limit=120/n1=FgRCTw9VBA4GCV5c/n2=Aw1fVhQKX15XAW5KXFI_/view=text/

search can also be narrowed by certain categories:
http://www.dmm.com/search/=/searchstr=谷口めぐ/limit=120/n1=FgRCTw9VBA4GCV5c/n2=Aw1fVhQKX15XAW5KXFI_/n3=EwBCUAlDEgMEW1AClvfHi8SFdw__/view=text/

Here, EwBCUAlDEgMEW1AClvfHi8SFdw__ means "Team A"

FgRCTw9VBA4GCV5c = AKB48 Group
Aw1fVhQKX15XAW5KXFI_ = REVIVAL!! ON DEMAND
Aw1fVhQKX15XAW5ZWFRSDw__ = AKB48



EwBCUAlDEgMEW18LCZOnlt3A1ngD/ = チームN3 (Team NIII)
http://www.dmm.com/lod/rod/-/list/=/group=akb48/sort=performance_date/view=text/

These aren't so much searches but if I'm going to do a proper ROD scraper they need to be dealt with
http://www.dmm.com/lod/rod/-/list/=/group=akb48/sort=performance_date/view=text/

But those are already dealt with by get_rod_performance_list ?

There's IDs like the above for groups as well as members


FQRCSglYUA4G25%2APlPi3i6M_ = 小熊倫実
FQRCSglYUA4G15%2C03fvVhaM_ = 荻野由佳
"""
import re
# import requests
from bs4 import BeautifulSoup


# _groups_to_teams = {
#     "akb48": ('a', 'k', 'f', 'ei', 'r'),
#     "ske48": ('s', 'k2', 'e', 'r2'),
#     "nmb48": ('n', 'm', 'b2', 'r4', 'ca'),
#     "hkt48": ('h', 'k4', 't2', 'r3', 'sf'),
#     "ngt48": ('n3', 'g', 'r5'),
#     "stu48": (),
# }
_cid_re = re.compile(r'/cid=(\w{3}48[\w\d]+\d{6}\d{2}[a-z]?)/')
_date_re = re.compile(r'(\d{4})年(\d+)月(\d+)日')
_pagenav_re = re.compile(r'/page=(\d+)/')


class SearchMixin:
    """
    Mainly for searching ROD stages, since the rest generally fit on one page and don't require actual searching.
    """
    def search(self, search_string):
        search_url = '/'.join((
            'https://www.dmm.com/search/=',
            'searchstr={}'.format(search_string),
            'limit=120',
            # limits searching to ROD somehow?
            'n1=FgRCTw9VBA4GCV5c/n2=Aw1fVhQKX15XAW5KXFI_', 
            # doesn't bother with images
            # 'view=text',
            ''
        ))
        # DEBUG
        print(search_url)
        r = self.session.get(search_url)
        if not r.ok:
            # TODO: handle status errors
            # also handle errors during the get (connection etc.)
            r.raise_for_status()

        soup = BeautifulSoup(r.text, 'lxml')

        page_count = _get_page_count(soup)
        print(page_count)

        for i in range(page_count):
            if i:
                search_url = '{}/page={}/'.format(url, i+1)
                r = self.session.get(search_url)
                if not r.ok:
                    print(search_url)
                    # TODO: handle status errors
                    # also handle errors during the get (connection etc.)
                    r.raise_for_status()
                soup = BeautifulSoup(r.text, 'lxml')

            # TODO: load additional pages
            # for item in soup.select('table#list > tr'):
            for item in soup.select('.tmb'):
                # skip the header row
                # if item.th:
                #     print('Skipping header row')
                #     continue
                yield _parse_search_result(item)


# TODO: move all this to a utils file?
# or merge this with the search module?
def _get_page_count(soup):
    pagenav = soup.select_one('div.list-boxpagenation').select('li')
    # TODO: make this more intelligent, e.g. check for li.terminal
    # if no li.terminal, take the second to last link

    # case 1: one page -- no page links
    if len(pagenav) == 1:
        return 1
    # case 2: two pages -- next button is the last page
    elif len(pagenav) == 3:
        return 2
    # case 3: three pages -- next button isn't the last page
    elif len(pagenav) == 4:
        return 3
    # case 4: 4 or more pages -- next and last buttons
    else:
        m = _pagenav_re.search(pagenav[-1].a['href'])
        if not m:
            print('Couldn\'t find page count:', pagenav[-1].a['href'])
            raise ValueError
        return int(m.group(1))


def _parse_date(title):
    m = _date_re.search(title)
    if not m:
        print('Failed to find date in title:', title)
        return ""
    return "{:04d}-{:02d}-{:02d}".format(*(int(e) for e in m.groups()))


def _parse_search_result(item):
    # results come in three columns, 
    # actually if I just use the view=image (i.e. the default) response I can use this as is
    # except for hd_available which doesn't seem to show up for ROD

    url = item.a['href']
    m = _cid_re.search(url)
    if not m:
        raise ValueError('Couldn\'t locate cid in {}'.format(url))
    cid = m.group(1)
    title = item.img['alt']

    # if '高画質' in item.text:
    #     hd_available = True
    # else:
    #     hd_available = False

    # rod entries also have a group icon that could be useful

    return dict(
        date=_parse_date(title),
        cid=cid,
        url=url,
        title=title,
        # hd_available=hd_available,
        image_url=item.img['src'],
    )