import requests
from .endpoints import PlaylistMixin, PerformanceListMixin, PlayerMixin, SearchMixin
import re
import time

cid_re = re.compile(r'(?P<group>\w+48)(?P<team>.+?)(?P<date>\d{6})(?P<index>\d{2})?\D*$')


# TODO: compare cid against social48 stage index to generate a title
# def build_title(info):
#     group, team, date, index = cid_re.match(info['cid']).groups()
#     title = '{} {} [{}] LOD [{}] 1080p DMM HD'.format(group.upper(), date, team.upper(), int(index))
#     event = info['title'].split('公演', 1)[-1].strip()
#     if event:
#         title = '{} ({})'.format(title, event)
#     return title

def build_title(info):
    group = info['group'].upper()
    title = info['title']
    cid = info['cid']
    return f'{group} {title} [{cid}]'


def get_date(info):
    m = cid_re.match(info['cid'])
    date = m.group(3)
    date = "20{}-{}-{}".format(date[:2], date[2:4], date[4:])
    return date


# TODO: actually implement API endpoints, which was the whole point of making a client class
# TODO: should the session always use the licenseUID if logged in?
class DMMClient(PlaylistMixin, PlayerMixin, PerformanceListMixin, SearchMixin): #
    def __init__(self,
                 config=None,
                 session=None,
                 ):
        if not session:
            self.session = requests.Session()
            # add cookies etc.
        else:
            self.session = session

        if not config or not isinstance(config, dict):
            self.config = {}
        else:
            self.config = config
        self.session.cookies.update(self.config.get('cookie', {}))
        self.session.headers.update(self.config.get('headers', {}))

    def get_latest_dmm_hd_playlists(self, group=None, since=None):
        """
        Get a dictionary listing the latest hd performances, optionally
        since a given date. Returns a list of {date:, title:, cid:, playlist:} dicts
        """
        # TODO: sanity test headers and cookies
        if not group:
            group = self.config.get('group')
            if not group:
                raise TypeError('You must provide a group, either in config or as an argument.')

        results = []

        for item in self.get_group_performance_list(group):
            # if not item['hd_available']:
            #     continue
            
            # TODO: set the date in get_group_performance_list
            date = get_date(item)
            if since and date <= since:
                continue

            # TODO: more robust error handling
            for error_count in range(5):
                try:
                    info = self.get_playlist_urls(item['cid'])[0]
                except ConnectionError:
                    time.sleep(error_count*error_count/2)
                    continue
                # *should* be "couldn't find args in text", but there might be other errors that inherit from ValueError
                except ValueError as e:
                    print('Failed to read page, are your cookies up to date?\n', e)
                    import sys
                    sys.exit(1)
                else:
                    break
            else:  # no break
                print('Failed to get playlists for', item['cid'])
                continue

            results.append(dict(
                date=date,
                title=build_title(item), 
                cid=item['cid'], 
                playlist=info['playlist']))

        return sorted(results, key=lambda x: (x['date'], x['cid']))





