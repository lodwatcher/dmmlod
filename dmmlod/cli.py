# subcommands
# setup: ask for basic information, generate config files
# set: set and save various config options, need to think about how to do this
# save: save video segments from dmm, decrypting if possible
# decrypt: decrypt saved video segments
# merge: combine decrypted video segments using ffmpeg
# - without ffmpeg simple concatenation corrupts some streams

# TODO: more advanced api usage
# e.g. login, getting playlists from html5 endpoint, etc.
import argparse
import os
import shutil
import yaml
import base64
import json


from . import config
from .api.client import DMMClient
from .video import merge_folder, decrypt_folder
from .utils import split_cookie_string, prune_cookies


def _get_key_from_args(args):
    if args.keyfile:
        with open(args.keyfile, 'rb') as infp:
            key = infp.read()
    elif args.key:
        key = base64.b64decode(args.key)
    else:
        key = None

    return key


def do_dmmhd(args):
    try:
        config_data = config.load(args.config_file)
    except FileNotFoundError:
        print('For best results, generate a config file using `dmmlod setup` first.')
        config_data = {}

    if 'cookie' not in config_data:
        print('No cookies found, API features will not work. Please set cookies using dmmlod set:',
              "\n    dmmlod set cookie='Cookie: app_uid=...'")
        return

    client = DMMClient(config=config_data)
    group = args.group or config_data.get('group')
    if not group:
        print('A group is required, either via --group or in the config file')
        return

    dmmhd = client.get_latest_dmm_hd_playlists(group=group, since=args.since)
    if args.output_file:
        outfp = open(args.output_file, 'w', encoding='utf8')
    else:
        outfp = None
    if args.format == 'json':
        if args.indent:
            indent = int(args.indent)
        else:
            indent = None
        print(json.dumps({stage['title']: stage['playlist'] for stage in dmmhd},
                         indent=indent, ensure_ascii=False), file=outfp)
    elif args.format == 'text':
        for key, val in ((stage['title'], stage['playlist']) for stage in dmmhd):
            print(key, val, sep='\n', end='\n\n', file=outfp)
    if outfp:
        outfp.close()


def do_save(args):
    try:
        data = config.load(args.config_file)
    except FileNotFoundError:
        print('For best results, generate a config file using `dmmlod setup` first.')
        data = {}

    key = _get_key_from_args(args)

    client = DMMClient(config=data)

    if args.license:
        client.config['licenseUID'] = args.license

    final_name = client.save_playlist(args.playlist, args.savedir, args.name,
                                      skip_exists=args.skip_exists, key=key)

    if args.merge:
        # ffmpeg = data.get('ffmpeg', 'ffmpeg')
        srcdir = os.path.join(args.savedir, final_name)
        destfile = os.path.join(args.savedir, '{}.ts'.format(final_name))
        merge_folder(srcdir, destfile)


def do_decrypt(args):
    srcdir = args.source_directory
    destdir = args.dest_directory
    os.makedirs(destdir, exist_ok=True)
    key = _get_key_from_args(args)

    decrypt_folder(srcdir, destdir, key=key)


def do_merge(args):
    srcdir = args.source_directory
    destfile = args.dest_file

    # try:
    #     ffmpeg = config.load(args.config_file).get('ffmpeg', 'ffmpeg')
    # except FileNotFoundError:
    #     print('Using default ffmpeg location. For best results, generate a config file using `dmmlod setup` first.')
    #     ffmpeg = 'ffmpeg'

    merge_folder(srcdir, destfile)


def do_set(args):
    data = config.load(args.config_file)
    data.update(args.options)
    if args.license:
        data['licenseUID'] = args.license

    config.save(data, args.config_file)


def do_schedule(args):
    # schedule(self, playlist, datestr=None, timestr=None, name=None, savedir=None)
    try:
        data = config.load(args.config_file)
    except FileNotFoundError:
        print('For best results, generate a config file using `dmmlod setup` first.')
        data = {}

    client = DMMClient(config=data)
    if args.license:
        client.config['licenseUID'] = args.license

    final_name = client.schedule(playlist=args.playlist, datestr=args.date, timestr=args.time,
                                 name=args.name, savedir=args.savedir)

    print('Scheduled recording of', final_name, 'completed.')


def do_setup(args):
    # ask for paths/urls/etc.
    # save config
    srcpath = args.config_file  # will search paths anyway if None
    if args.use_standard_config_path:
        savepath = config.find_config()
    else:
        savepath = srcpath or config.find_config()

    b_override = args.override

    data = config.load(srcpath, ignore_errors=True)
    if data:
        print('Loaded config from {}'.format(srcpath))

    if args.options:
        data.update(args.options)

    print('DMM LOD Archival Tool needs some information about your system and what you will be recording:')
    if b_override or not data.get('save_directory'):
        default_path = data.get('save_directory') or args.savedir
        choice = input('Where do you want to save downloaded videos? [{}]: '.format(default_path))
        if not choice:
            data['save_directory'] = os.path.expanduser(default_path)
        elif choice.startswith('~'):
            print('Converting user path to absolute...')
            data['save_directory'] = os.path.expanduser(choice)
        elif choice.startswith('/') or (len(choice) > 1 and choice[1] == ':'):
            print('Using absolute path...')
            data['save_directory'] = choice
        else:
            print('Converting relative path to absolute...')
            data['save_directory'] = os.path.abspath(choice)

    if b_override or not data.get('group'):
        choice = input('Do you wish to record for one specific group? [y/N]: ')
        if choice.lower().startswith('y'):
            default_group = data.get('group') or 'akb48'
            choice = input('Which group will you be recording? [{}]: '.format(default_group))
            if not choice:
                data['group'] = default_group
            else:
                data['group'] = choice

    # TODO: more robust cookie management
    if b_override or not data.get('licenseUID'):
        if args.license:
            license = args.license
        else:
            print('Please obtain your current licenseUID from a browser and enter it here: ')
            license = input()
        # TODO: check that it's the right length etc.
        data['licenseUID'] = license

    # TODO: custom filename patterns for each playlist
    if b_override or not data.setdefault('playlists', {}):
        # The default behaviour assumes one is only trying to record for one group per config file
        print("The playlist urls for live performances seem to be fairly constant. You can store them\n"
              "in the config file to access by name instead of pasting the whole url.")
        choice = input('Do you wish to do this now? [y/N]: ').lower()
        while choice.startswith('y'):
            choice = input('Enter a convenient name for the playlist (e.g. livehd): ')
            name = choice
            if name in data['playlists']:
                print('This name already exists, with the following URL:\n', data['playlists'][name])
                choice = input('Do you wish to overwrite? [y/N]: ').lower()
                if not choice.startswith('y'):
                    continue
            print('Enter the full playlist URL: ')
            # TODO: validate
            url = input()
            data['playlists'][name] = url
            print('{}: {}'.format(name, url))
            choice = input('Do you wish to add another playlist? [y/N]: ').lower()

    # TODO: other necessary headers?
    if b_override or not data.get('headers', {}).get('User-Agent'):
        print('No User-Agent header found. Please copy and paste the user-agent header used by your browser: ')
        ua_str = input()
        if ua_str:
            data.setdefault('headers', {})['User-Agent'] = ua_str
        else:
            # Client supplies the default if none is in config
            print('No User-Agent was provided, the default will be used.')

    # look for ffmpeg
    # if b_override or not data.get('ffmpeg'):
    #     ffmpeg_path = shutil.which('ffmpeg')
    #     if not ffmpeg_path:
    #         print('Cannot locate FFmpeg. Please provide the path to the ffmpeg executable.')
    #         data['ffmpeg'] = input()
    #     else:
    #         data['ffmpeg'] = 'ffmpeg'  # on PATH
    #
    # # check that ffmpeg is runnable
    # while not shutil.which(data.get('ffmpeg')):
    #     print('Cannot locate or execute ffmpeg, please supply the correct path: ')
    #     data['ffmpeg'] = input()

    print('Creating directories...')
    os.makedirs(data['save_directory'], exist_ok=True)
    os.makedirs(os.path.dirname(savepath), exist_ok=True)

    print('Saving config to {}...'.format(savepath))
    # TODO: allow saving to a different location?
    config.save(data, savepath)
    # TODO: try linking config to download directory


def do_get(args):
    data = config.load(args.config_file)

    def print_field(field):
        print('{}:'.format(field))
        if isinstance(data[field], dict):
            if args.show_full:
                for key, val in data[field].items():
                    print('{}: {}'.format(key, val))
            else:
                print(*sorted(data[field].keys()), sep='\n')
        elif isinstance(data[field], list):
            print(*sorted(data[field]), sep='\n')
        else:
            print(data[field])
        print()

    for field in args.fields:
        if field == 'all':
            for key in data.keys():
                print_field(key)
        else:
            print_field(field)


class ConfigOptionParser(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        result = {}
        for option in values:
            key, val = option.split('=', 1)
            val = val.strip()

            # TODO: better support nested options, e.g. playlists, headers
            try:
                data = yaml.safe_load(val)
            except yaml.YAMLError as e:
                data = val
            if key == 'cookie' and isinstance(val, str):
                cookies = split_cookie_string(val)
                prune_cookies(cookies)
                data = cookies
                result['licenseUID'] = cookies.get('licenseUID')
            result[key] = data
        setattr(namespace, 'options', result)


def build_parser():
    parser = argparse.ArgumentParser(prog="DMM-LOD-Archival-Tool",
                                     description="Download and process live streams from DMM.com")
    parser.add_argument('--license', '-L', help='licenseUID cookie')
    parser.add_argument('--config-file', '-C', help='Config file to load from or save to')
    parser.add_argument('--key', '-k', help='Base64 encoded decryption key')
    parser.add_argument('--keyfile', '-K', help='Path to decoded decryption key file')
    parser.add_argument("--savedir", "-o", help="Output directory for saved files")

    sp = parser.add_subparsers()

    save_parser = sp.add_parser('save', help='Save files from a DMM playlist (live or recorded)')
    save_parser.add_argument("playlist", help="Either the URL of the playlist.m3u8, "
                                              "or a playlist name to load from config, like \"livehd\". "
                                              "Automatically downloads the best available stream. "
                                              "Note that fixed camera and hd use different playlist "
                                              "urls from the default sd stream")
    save_parser.add_argument("--name", "-n", help="Stream name, used as a folder name. If not provided "
                                                  "Will be generated from group, date, and playlist.")
    save_parser.add_argument("--skip-exists", "-x", action="store_true",
                             help="Skip existing files (use to patch failed downloads of VODs)")
    # -D for decrypt is redundant, since save will do that automatically if possible, and if it isn't there's
    # no point in having such an option.
    save_parser.add_argument("--merge", "-M", action="store_true",
                             help="Merge on completion. Only possible with either a valid license or key/keyfile. "
                                  "Skipped if there are errors during the download.")
    save_parser.add_argument("--savedir", "-o", help="Output directory for saved files")
    save_parser.set_defaults(func=do_save)

    decrypt_parser = sp.add_parser('decrypt', help='Decrypt files saved from DMM.')
    decrypt_parser.add_argument('source_directory', help='Directory holding the saved, encrypted TS files')
    decrypt_parser.add_argument('dest_directory', help='Directory to save the decrypted files to.')
    decrypt_parser.set_defaults(func=do_decrypt)

    # TODO: allow decryption during merge???
    merge_parser = sp.add_parser('merge', help='Concatenate a previously decrypted saved stream',
                                 epilog='The resulting file should still be run through mkvmerge '
                                        'to remove filler bytes.')
    # lod.py merge source_directory destination_file -k key
    merge_parser.add_argument('source_directory', help='Directory holding the saved, decrypted TS files')
    merge_parser.add_argument('dest_file', help='File name for merged TS file', nargs='?', default=None)
    merge_parser.set_defaults(func=do_merge)

    set_parser = sp.add_parser('set', help='Set config options.')
    set_parser.add_argument('options', nargs='*', action=ConfigOptionParser,
                            help='Series of key=value pairs. '
                                 '\nUse cookie="<cookie string from browser>" to set cookies for accessing '
                                 'the DMM API')
    set_parser.set_defaults(func=do_set)

    setup_parser = sp.add_parser('setup', help='Create folders and ask for necessary information.')
    setup_parser.add_argument('options', nargs='*', action=ConfigOptionParser, help='Series of key=value pairs')
    setup_parser.add_argument('--override', action='store_true', help='Ask for all config options, '
                                                                      'even if already present')
    setup_parser.add_argument('--use-standard-config-path', '-S', action='store_true',
                              help='Save to a standard path, even if a config file is given.')
    setup_parser.add_argument("--savedir", "-o", help="Output directory for saved files")
    setup_parser.set_defaults(func=do_setup)

    schedule_parser = sp.add_parser('schedule', help='Schedule a recording')
    schedule_parser.add_argument("playlist", help="Either the URL of the playlist.m3u8, "
                                                  "or a playlist name to load from config, like \"livehd\". "
                                                  "Automatically downloads the best available stream. "
                                                  "Note that fixed camera and hd use different playlist "
                                                  "urls from the default sd stream")
    schedule_parser.add_argument("--date", "-d", help="Date of the recording, in YYYY-MM-DD format")
    schedule_parser.add_argument("--time", "-t", help="Time of the recording, in HH:mm format")
    schedule_parser.add_argument("--name", "-n", help="Stream name, used as a folder name. If not provided "
                                                      "Will be generated from group, date, and playlist.")
    schedule_parser.add_argument("--savedir", "-o", help="Output directory for saved files")
    schedule_parser.set_defaults(func=do_schedule)

    get_parser = sp.add_parser('get', help='Display config options')
    get_parser.add_argument('--show-full', '-F', action='store_true',
                            help='Display full keys and values for dict items '
                                 '(otherwise shows only keys)')
    get_parser.add_argument('fields', nargs='+', help="List of config fields to display")
    get_parser.set_defaults(func=do_get)

    dmmhd_parser = sp.add_parser('dmmhd', help='Latest DMM HD playlists')
    dmmhd_parser.add_argument('--since', help='Find only playlists after this date (YYYY-MM-DD)')
    dmmhd_parser.add_argument('--group', help='Find playlists from this group (default set in config)')
    dmmhd_parser.add_argument('--format', choices=('json', 'text'), default='json',
                              help='Format for output, either json or text')
    dmmhd_parser.add_argument('--indent', help='Indentation to use for JSON output, default: None')
    dmmhd_parser.add_argument('--output-file', '--of', help="File to write to (helpful on Windows)")
    dmmhd_parser.set_defaults(func=do_dmmhd)
    # TODO: list available playlists
    return parser


def main():
    # this is what will be called by dmmlod once it is installed
    parser = build_parser()
    args = parser.parse_args()
    args.func(args)
