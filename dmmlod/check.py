import os
import glob
import json
from subprocess import check_output, DEVNULL, CalledProcessError

# path to ffprobe, todo: make configurable
FFPROBE = 'ffprobe'


def probe_video(filename, stream='v', entries=()):
    try:
        results = check_output([
            FFPROBE,
            '-loglevel', '16',
            '-show_entries', 'stream={}'.format(','.join(entries)),
            '-select_streams', stream,
            '-i', filename,
            '-of', 'json'
        ],
            universal_newlines=True,
            stderr=DEVNULL,
            stdin=DEVNULL
        )
    except CalledProcessError:
        return None
    else:
        try:
            return json.loads(results)['streams']
        except IndexError:
            return None


def check_file(path, logfile=None):
    if not logfile:
        logfile = f'{os.path.dirname(path)}_check.log'
    
    # def get_start_seconds(file):
    #     time_str = _split_filename(file)[1]
    #     hours, minutes = int(time_str[:2]), int(time_str[2:4])
    #     try:
    #         seconds = int(time_str[4:6])
    #     except ValueError:
    #         seconds = 0
    #     return hours * 60 * 60 + minutes * 60 + seconds + 1

    filename = os.path.basename(path)

    # try:
    #     start_time = get_start_seconds(path)
    # except ValueError:
    #     # TODO: handle None-value later in the code when start_time is actually checked
    #     start_time = None

    new_video = {
        'file': {
            'name': filename,
            'size': os.path.getsize(path)
        }
    }

    probe_results = probe_video(
    	path, 
    	stream="", 
    	entries=(
    		"codec_name",
			"codec_type",
			"duration",
			"height",
			"avg_frame_rate",
			"bit_rate",
			"nb_frames",
		)
    )

    # ignore empty results (CalledProcessError or unreadable json)
    if probe_results:
        temp = {}
        try:
            for e in probe_results:
                stream_type = e.pop('codec_type')
                temp[stream_type] = e
            v_info, a_info = temp['video'], temp['audio']
        except KeyError as e:
            with open(logfile, 'a', encoding='utf8') as outfp:
                print('Probe of {} failed'.format(filename), e, file=outfp)
            new_video['valid'] = False
        else:
            new_video['video'] = {'duration': float(v_info.get('duration', 0)),
                                  'height': int(v_info.get('height', 0)),
                                  'avg_frame_rate': v_info.get('avg_frame_rate', ""),
                                  'bit_rate': int(v_info.get('bit_rate', 0)),
                                  'frames': int(v_info.get('nb_frames', 0))}
            new_video['audio'] = {'duration': float(a_info.get('duration', 0)),
                                  'bit_rate': int(a_info.get('bit_rate', 0))}
            if (new_video['video']['duration'] < 0.001):
                with open(logfile, 'a', encoding='utf8') as outfp:
                    print('{} video is too short'.format(path), file=outfp)
                new_video['valid'] = False
            # elif not (new_video['video']['height'] in GOOD_HEIGHTS or 'Kimi Dare' in member_name):
            #     with open(logfile, 'a', encoding='utf8') as outfp:
            #         print('{} has bad video height: {}'.format(file, new_video['video']['height']), file=outfp)
            #     new_video['valid'] = False
            # elif (new_video['video']['height'] in BAD_HEIGHTS
            #         and new_video['video']['bit_rate'] < 10000
            #         and new_video['video']['duration'] < 90):
            #     # black screen videos tend to be about 7200 bps, last for ~60 seconds, and have a height of 540
            #     with open(logfile, 'a', encoding='utf8') as outfp:
            #         print('{} video matches profile of a black screen'.format(path), file=outfp)
            #     new_video['valid'] = False
            else:
                new_video['valid'] = True
    else:
        new_video['valid'] = False
    return new_video


def check_folder(target, ext='mp4'):
    files = sorted(glob.glob(f'{target}/*.{ext}'))
    results = []
    for file in files:
        r = check_file(file)
        results.append(r)
        # do a desync check automatically
        try:
            diff = r['video']['duration'] - r['audio']['duration']
        except KeyError as e:
            print('Missing key for', file, e)
            continue
        if not abs(diff) < 2.0:
            print(file, diff, sep='\n')

    return results