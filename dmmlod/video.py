import binascii
import glob
import os

from Crypto.Cipher import AES

from dmmlod.utils import get_media_sequence, sort_segments
from dmmlod.constants import DEFAULT_CHUNKSIZE


def decrypt_folder(src, dest, key=None):
    """
    Decrypts all videos in src folder and outputs to dest folder

    NOTE: This method is not asynchronous

    TODO:
    - detect and decode base64 encoded string key ?

    :param src: Folder to decrypt videos from
    :param dest: Folder to save decrypted videos to
    :param key: len 16 or len 32 bytestring
    :return:
    """
    # TODO: read other segment name formats
    files = glob.glob('{}/media_*.ts'.format(src))
    for file in files:
        filename = os.path.basename(file)
        try:
            decrypt_video(os.path.join(src, filename), os.path.join(dest, filename), key=key)
        except ValueError as e:
            print(e)
            print('Problem file:', file)


def decrypt_video(infile, outfile, key, iv=None):
    """
    Decrypt single videos

    NOTE: This method is not asynchronous

    :param infile:
    :param outfile:
    :param key:
    :param iv:
    :return:
    """
    if not iv:
        iv = hex(get_media_sequence(infile))[2:]
        iv = "0" * (32 - len(iv)) + iv
        iv = binascii.unhexlify(iv)

    crypt_mode = AES.MODE_CBC
    decryptor = AES.new(key, crypt_mode, IV=iv)
    size_written = 0
    with open(infile, 'rb') as infp:
        with open(outfile, 'wb') as outfp:
            # https://stackoverflow.com/a/4566523
            for cryptchunk in iter(lambda: infp.read(DEFAULT_CHUNKSIZE), b""):
                plainchunk = decryptor.decrypt(cryptchunk)
                size_written += outfp.write(plainchunk)
    return size_written


def merge_folder(srcdir, destfile=None):
    """
    Merges decrypted videos into a single file using ffmpeg

    NOTE: This method is not asynchronous

    :param ffmpeg: location of the ffmpeg executable
    :param srcdir:
    :param destfile:
    :return:
    """
    # import shutil
    if not destfile:
        if srcdir.endswith('/') or srcdir.endswith('\\'):
            srcdir = srcdir[:-1]
        destfile = '{}.ts'.format(srcdir)

    files = glob.glob('{}/media_*.ts'.format(glob.escape(srcdir)))
    sort_segments(files)

    bytes_written = 0
    total_size = sum(os.path.getsize(f) for f in files)
    print('Merging to {}'.format(destfile))
    print('Total Bytes:  ', total_size)
    with open(destfile, 'wb') as outfp:
        for i, file in enumerate(files):
            with open(file, 'rb') as infp:
                while True:
                    chunk = infp.read(DEFAULT_CHUNKSIZE)
                    if not chunk:
                        break
                    bytes_written += outfp.write(chunk)
            if i % 10 == 0:
                print('Bytes Written:', bytes_written, end='\r', flush=True)
                # shutil.copyfileobj(infp, outfp)
    # final bytes written
    print('Bytes Written:', bytes_written, end='\n', flush=True)

    return destfile
