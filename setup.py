from setuptools import setup

deps = [
    "m3u8",
    "pycryptodome",
    "pyyaml",
    "appdirs",
    "requests",
    "beautifulsoup4",
    "lxml"
]

setup(
    name='dmmlod',
    version='0.0.4dev',
    packages=['dmmlod',],
    license='MIT',
    long_description=open('README.md').read(),
    install_requires=deps,
    python_requires=">=3.5.3, <4",
    entry_points={
        "console_scripts": ["dmmlod=dmmlod.cli:main"]
    }
)